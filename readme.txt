Hi there!
For this project to work you'll need to install sqlite on your machine: https://www.sqlite.org/download.html

"""
As of 12/9/2022, I could not write unittests and logging functionality to the code.
I've been busy with the finals.
You don't need to ingest the data I've included the db file.
"""
Here is the short description of each file:

Problem_1.py : It just has the DDL Statements that I used to create for weather data and yield data.
Problem_2_data_extraction.py - It has the helper functions for problem 2, it basically loads the text files into pandas dataframes.
Problem_2_ingestion.py - This has the SQL queries and db code to ingest the data, I have not included the logging functionality. But the the ingestion took around 2 hours for the 1.7 million records.
Problem_3_analysis.py - This mainly has the pandas code used to calculate different values and then, the ingestion code at the end.
Problem_4.py - This has the Flask API code along with pagination.
db_functions.py - It is a helper file, that has all the SQL queries used by the endpoints in problem_4.py.