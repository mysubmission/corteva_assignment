"""
This mainly has the pandas code used to calculate different values and then, the ingestion code at the end.
"""

import pandas as pd
import sqlalchemy

DBI_URI = 'sqlite:///database/corteva.db'

engine = sqlalchemy.create_engine(DBI_URI, echo=False)

conn = engine.connect()




def insert_df_to_db(df):
    for i in df.iterrows():

        params = (str(i[1][0]), i[1][1], i[1][2], i[1][3])

        command = "INSERT INTO stats (id, mean_max_temp, mean_min_temp, cum_prec) VALUES ( ?, ?, ?, ?)"

        try:
            conn.execute(command, params)
        except Exception as e:
            print("Data already present...")
            return


if __name__ == "__main__":
    # Uncomment to run the DDL Statement
    # conn.execute(ddl_command_1) # uncomment to create stats table.
    #
    # ddl_command_1 = """
    # CREATE TABLE IF NOT EXISTS stats  (
    # id TEXT PRIMARY KEY NOT NULL,
    # mean_max_temp REAL,
    # mean_min_temp REAL,
    # cum_prec REAL
    # )
    #
    # """

    # Fetching all the weather data
    rows_count_weather_data = conn.execute("select * from weatherData").fetchall()

    # Pushing weather data into a pandas dataframe
    df = pd.DataFrame(rows_count_weather_data, columns=["id", "date", "maxTemp", "minTemp", "prec"])

    # Handling the Null values
    df = df.replace(-9999, None)

    # Creating an id column StationID_Year for easier processing
    df["id"] = df.id.str[0:11] + "_" + df.id.str[11:15]

    # Calculating averages of max and min temp by having them grouped by id (Weather Station & Year)
    avg_min_max_temp_df = df.groupby(['id'], as_index=False)[["maxTemp", "minTemp"]].mean()

    # Getting cumulative precipitation sum
    cum_precip_df = df.groupby(['id'], as_index=False)[["prec"]].sum()

    # Outer join to make it a one dataframe
    avg_min_max_temp_df = pd.merge(avg_min_max_temp_df, cum_precip_df, on='id', how='outer')

    # Getting values in df according to right units
    avg_min_max_temp_df['prec'] = avg_min_max_temp_df['prec'] * 0.1
    avg_min_max_temp_df['maxTemp'] = avg_min_max_temp_df['maxTemp'] * 0.1
    avg_min_max_temp_df['minTemp'] = avg_min_max_temp_df['minTemp'] * 0.1

    # Rounding the values to one decimal place
    avg_min_max_temp_df = avg_min_max_temp_df.round(1)

    # Renaming columns
    avg_min_max_temp_df.rename(columns={"maxTemp": "meanMaxTemp", "minTemp": "meanMinTemp", "prec": "cumPrec"},
                               inplace=True)

    # Data Ingestion
    insert_df_to_db(avg_min_max_temp_df)

    #
    # avg_min_max_temp_df.to_csv("Trial.csv", index=False)
