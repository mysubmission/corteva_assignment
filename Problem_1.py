ddl_command_1 = """
CREATE TABLE IF NOT EXISTS weatherData  (
id TEXT PRIMARY KEY NOT NULL,
date TEXT,
max_temp REAL,
min_temp REAL,
precipitation REAL
)

"""

ddl_command_2 = """

CREATE TABLE IF NOT EXISTS yieldData  (
year TEXT PRIMARY KEY NOT NULL,
yield INTEGER
)

"""