import pandas as pd


def file_to_df(filepath, check):
    """
    This function extract data from file and load it into pandas dataframe
    :param filepath:
    :param filename:
    :param check: 1 for weatherData, 2 for yieldData
    :return: pandas dataframe

    """

    if check == 1:
        df = pd.read_csv(filepath, sep="\t",header=None)  # Reading .txt file into dataframe
        filename = filepath[8:len(filepath) - 4]
        df.columns = ["date", "maxTemp", "minTemp", "prec"]
        df['uniqueId'] = filename + df["date"].astype(str)  # Creating uniqueID for using it as primary key
        return df

    elif check == 2:
        df = pd.read_csv(filepath, sep='\t', header=None)  # Reading .txt file into dataframe

        df.columns = ["year", "yield"]
        return df



# print(file_to_df("wx_data/USC00110072.txt", "USC00110072"))
