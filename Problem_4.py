"""
This has the Flask API code along with pagination.

"""
from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from db_functions import get_stats, get_weather, get_yield

app = Flask(__name__)
api = Api(app)

"""
Pagination is implemented through LIMIT and OFFSET query string params
"""

class WeatherData(Resource):
    def get(self):
        station_id = request.args.get('station_id')
        date = request.args.get('date')

        limit = request.args.get('limit')
        offset = request.args.get('offset')

        if limit is None:
            limit = 5
        if offset is None:
            offset = 0
        res = {"data": "{}".format(get_weather(station_id, date, limit, offset))}
        return jsonify(res)


class YieldData(Resource):
    def get(self):
        year = request.args.get('year')
        limit = request.args.get('limit')
        offset = request.args.get('offset')
        if limit is None:
            limit = 5
        if offset is None:
            offset = 0
        res = {"data": "{}".format(get_yield(year, limit, offset))}
        return jsonify(res)


class Stats(Resource):
    def get(self):
        station_id = request.args.get('station_id')
        year = request.args.get('year')
        limit = request.args.get('limit')
        offset = request.args.get('offset')
        if limit is None:
            limit = 5
        if offset is None:
            offset = 0

        print(year, limit, offset)
        res = {"data": "{}".format(get_stats(station_id, year, limit, offset))}
        return jsonify(res)


api.add_resource(WeatherData, "/weather")
api.add_resource(YieldData, "/yield")
api.add_resource(Stats, "/weather/stats")

if __name__ == "__main__":
    app.run(debug=True)

# http://127.0.0.1:5000/weather/stats?station_id=USC00112140&year=1985
