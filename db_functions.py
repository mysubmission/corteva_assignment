"""

It is a helper file, that has all the SQL queries used by the endpoints in problem_4.py.

"""
import json
import collections
import sqlite3

conn = sqlite3.connect('database/corteva.db', check_same_thread=False)


def get_weather(weather_station=None, date=None, limit=5, offset=0):  # get weather data
    if weather_station is None and date is None:
        query = """SELECT * from weatherData LIMIT {} OFFSET {}""".format(limit, offset)
    elif weather_station is None and date is not None:
        query = """SELECT * from weatherData where date = {} LIMIT {} OFFSET {}""".format(date, limit, offset)
    elif weather_station is not None and date is None:
        query = """SELECT * from weatherData where id LIKE '%{}%' LIMIT {} OFFSET {}""".format(weather_station, limit,
                                                                                               offset)
    else:
        query = """SELECT * from weatherData where id == '{}{}' LIMIT {} OFFSET {} """.format(weather_station, date,
                                                                                              limit, offset)

    results = conn.execute(query).fetchall()
    list_results = []
    for result in results:
        result_dict = collections.OrderedDict()
        result_dict['id'] = result[0]
        result_dict['date'] = result[1]
        result_dict['maxTemp'] = result[2]
        result_dict['minTemp'] = result[3]
        result_dict['prec'] = result[4]
        list_results.append(result_dict)

    return json.dumps(list_results)


def get_stats(weather_station=None, year=None, limit=5, offset=0):  # get weather stats data
    if weather_station is None and year is None:
        query = """SELECT * from stats LIMIT {} OFFSET {}""".format(limit, offset)
    elif weather_station is None and year is not None:
        query = """SELECT * from stats where date = {} LIMIT {} OFFSET {}""".format(year, limit, offset)
    elif weather_station is not None and year is None:
        query = """SELECT * from stats where id LIKE '%{}%' LIMIT {} OFFSET {} """.format(weather_station, limit,
                                                                                          offset)
    else:
        query = """SELECT * from stats where id == '{}_{}' LIMIT {} OFFSET {} """.format(weather_station, year, limit,
                                                                                         offset)

    results = conn.execute(query).fetchall()

    list_results = []
    for result in results:
        result_dict = collections.OrderedDict()
        result_dict['id'] = result[0]
        result_dict['meanMaxTemp'] = result[1]
        result_dict['meanMinTemp'] = result[2]
        result_dict['cumPrec'] = result[3]
        list_results.append(result_dict)

    return json.dumps(list_results)


def get_yield(year=None, limit=5, offset=0):  # get weather stats data
    if year is None:
        query = """SELECT * from yieldData LIMIT {} OFFSET {}""".format(limit, offset)
    else:
        query = """SELECT * from yieldData where year = {} LIMIT {} OFFSET {}""".format(year, limit, offset)
        print(query)
    results = conn.execute(query).fetchall()

    list_results = []
    for result in results:
        result_dict = collections.OrderedDict()
        result_dict['year'] = result[0]
        result_dict['yield'] = result[1]
        list_results.append(result_dict)

    return json.dumps(list_results)


print(get_yield())
# print(get_stats(weather_station='USC00112140'))
#
# print(get_weather())
