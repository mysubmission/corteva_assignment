"""

This file will fetch data from the given text files and ingest it into the sqlite database.

"""

import os
import sqlalchemy
from Problem_2_data_extraction import file_to_df
import time


def append_dataframes_to_list(directory, data_check):
    """

    :param directory: where text files are present
    :param data_check: to know what table are we going to put data in
    :return: list of all the dataframes consisting text files
    """
    df_list = []
    dir_items = os.listdir(directory)
    for item in dir_items:
        df_list.append(file_to_df("{}/{}".format(directory, item), data_check))

    return df_list


def insert_dfs_to_db(dflist, data_check):
    """
    :param dflist: list of pandas dataframes
    :param data_check: 1 -> weather data, 2 -> yield data
    :return:
    """

    for df in dflist:
        for i in df.iterrows():
            if data_check == 1:
                params = (str(i[1][4]), str(i[1][0]), i[1][1], i[1][2], i[1][3])
                command = "INSERT INTO weatherData (id, date, max_temp, min_temp, precipitation ) VALUES ( ?, ?, ?, ?, ?)"
            elif data_check == 2:
                params = (str(i[1][0]), i[1][1])

                command = "INSERT INTO yieldData (year, yield) VALUES {}".format(params)

            try:
                engine.execute(command)
            except Exception as e:
                print("Data already present...")
                return


if __name__ == "__main__":
    # get the start time
    st = time.time()

    DBI_URI = 'sqlite:///database/corteva.db'
    engine = sqlalchemy.create_engine(DBI_URI, echo=False)

    conn = engine.connect()

    # Uncomment to insert dfs to respective tables

    # list_dfs_weather_data = append_dataframes_to_list("wx_data", 1)
    # insert_dfs_to_db(list_dfs_weather_data, 1)
    #
    # list_dfs_yield_data = append_dataframes_to_list("yld_data", 2)
    # insert_dfs_to_db(list_dfs_yield_data, 2)

    end = time.time()

    print("Time to ingest data: ", end - st)

    # # rows_count_weather_data = conn.execute("select * from weatherData").fetchall()
    # rows_count_yield_data = conn.execute("select * from yieldData").fetchall()
    #
    # print(rows_count_yield_data)
